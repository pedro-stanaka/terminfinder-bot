#!/usr/bin/env ruby
# make sure you the watir gem installed -> gem install watir
require 'watir'
require 'telegram/bot'
require_relative 'os_utils'


########## Configuration ###########
service_id = "327537"
office_list = "122231,122238,122243,122260"
@telegram_enabled = false
####################################


@url = "https://service.berlin.de/terminvereinbarung/termin/tag.php?termin=1&anliegen[]=%{service_id}&dienstleisterlist=%{office_list}&herkunft=1" % { service_id: service_id, office_list: office_list }

Telegram.bots_config = {
  default: ENV["TELEGRAM_BOT_TOKEN"]
}
Telegram.bot.get_updates
Telegram.bot == Telegram.bots[:default] # true


def log (message) puts "  #{message}" end
def success (message) puts "+ #{message}" end
def fail (message) puts "- #{message}" end
def notify (message)
  success message.upcase

  notify_linux(message) if OS.linux?
  notify_macos(message) if OS.macos?

  notify_telegram if @telegram_enabled

  log "Notified telegram and shell..."
rescue StandardError => e
  fail "An error occurred while trying to notify... Message: %s" % e.message
end

def notify_linux(message)
  success message.upcase
  system 'notify-send "Termin disponivel! Corre pro site!!!!" "%s"' % message
end

def notify_macos(message)
  system 'osascript -e \'display notification "Corre pro site! %s" with title "Termin disponivel!"\'' % message
end

def notify_telegram
  Telegram.bot.send_message(chat_id: "@TerminFinder", text: "Termin disponivel! Corre pro site! %s" % @url)
end

def appointmentAvailable? (b)
  puts '-'*80
  log 'Trying again'
  b.goto @url
  log 'Page loaded'
  link = b.element css: '.calendar-month-table:first-child td.buchbar a'
  if link.exists?
    link.click
    notify 'An appointment is available.'
    log 'Enter y to keep searching or anything else to quit.'
    return gets.chomp.downcase != 'y'
  else
    fail 'No luck this time.'
    return false
  end
rescue StandardError => e
  fail 'Error encountered.'
  puts e.inspect
  return false
end

# default is firefox but it doesn't work for some reasons
b = Watir::Browser.new :chrome
until appointmentAvailable? b
  sleep_time = rand(60..90)
  log 'Sleeping %d...' % sleep_time
  sleep sleep_time
end
